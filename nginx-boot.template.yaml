apiVersion: v1
kind: Template
labels:
  template: nginx-java
metadata:
  annotations:
    description: An example Nginx HTTP server and a reverse proxy (nginx) application backed by Java
      that serves static content. For more information about using this template,
      including OpenShift considerations, see https://gitlab.com/changdukkim/nginx-boot/README.md
    iconClass: icon-nginx
    openshift.io/display-name: Nginx HTTP server and a reverse proxy and OpenJDK
    openshift.io/documentation-url: https://gitlab.com/changdukkim/nginx-boot
    openshift.io/long-description: This template defines resources needed to develop
      a static application served by Nginx HTTP server and a reverse proxy (nginx)
      and Java(OpenJDK) including a build configuration and application deployment configuration.
    tags: quickstart,nginx,springboot
  name: nginx-java
objects:
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      description: The web server's http port.
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    ports:
    - port: 8080
      targetPort: 8080
    selector:
      deploymentConfig: ${APPLICATION_NAME}
- apiVersion: v1
  id: ${APPLICATION_NAME}-http
  kind: Route
  metadata:
    annotations:
      description: Route for application's http service.
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    to:
      name: ${APPLICATION_NAME}
- apiVersion: v1
  kind: BuildConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}-backend
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${APPLICATION_NAME}-backend:latest
    source:
      git:
        ref: ${SOURCE_REF}
        uri: ${SOURCE_URL}
      type: Git
    strategy:
      sourceStrategy:
        forcePull: true
        from:
          kind: ImageStreamTag
          name: redhat-openjdk18-openshift:latest
          namespace: openshift
      type: Source
    triggers:
    - github:
        secret: kJZLvfQr3hZg
      type: GitHub
    - generic:
        secret: kJZLvfQr3hZg
      type: Generic
    - imageChange: {}
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: BuildConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}-frontend
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${APPLICATION_NAME}-frontend:latest
    source:
      git:
        ref: ${SOURCE_REF}
        uri: ${SOURCE_URL}
      type: Git
    strategy:
      sourceStrategy:
        forcePull: true
        from:
          kind: ImageStreamTag
          name: nginx:latest
          namespace: openshift
      type: Source
    triggers:
    - github:
        secret: kJZLvfQr3hZg
      type: GitHub
    - generic:
        secret: kJZLvfQr3hZg
      type: Generic
    - imageChange: {}
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    replicas: 2
    selector:
      deploymentConfig: ${APPLICATION_NAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          application: ${APPLICATION_NAME}
          deploymentConfig: ${APPLICATION_NAME}
        name: ${APPLICATION_NAME}
      spec:
        containers:
        - env:
          image: ${APPLICATION_NAME}-frontend
          imagePullPolicy: Always
          name: ${APPLICATION_NAME}-frontend
          ports:
          - containerPort: 8080
            name: http
            protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 30
            timeoutSeconds: 3
          readinessProbe:
            httpGet:
              path: /
              port: 8080
            initialDelaySeconds: 3
            timeoutSeconds: 3
        - env:
          image: ${APPLICATION_NAME}-backend
          imagePullPolicy: Always
          name: ${APPLICATION_NAME}-backend
          ports:
          - containerPort: 18080
            name: http
            protocol: TCP
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${APPLICATION_NAME}-backend
        from:
          kind: ImageStream
          name: ${APPLICATION_NAME}-backend
      type: ImageChange
    - type: ConfigChange
parameters:
- description: The name for the application.
  name: APPLICATION_NAME
  required: true
  value: tasks
- description: Git source URI for application
  name: SOURCE_URL
  required: true
  value: https://github.com/openshiftdemos/openshift-tasks
- description: Git branch/tag reference
  name: SOURCE_REF
  value: master
